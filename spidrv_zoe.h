
#ifndef SPIDRV_ZOE_H
#define SPIDRV_ZOE_H

/***************************************************************************//**
 * Initialize gnss_sensor
 ******************************************************************************/
void initUSART1 (void);
void spidrv_app_zoe_init(void);

/***************************************************************************//**
 * functions of gnss_sensor
 ******************************************************************************/
void spidrv_app_zoe_action();

#endif  // SPIDRV_ZOE_H
