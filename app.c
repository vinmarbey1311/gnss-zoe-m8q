/***************************************************************************//**
 * @file
 * @brief Top level application functions
 *******************************************************************************
 * # License
 * <b>Copyright 2020 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/
#include "ustimer.h"
#include "sl_simple_led.h"
#include "sl_simple_led_instances.h"



#include "spidrv_zoe.h"

#ifndef LED_INSTANCE
#define LED_INSTANCE               sl_led_led0
#endif
#ifndef TOOGLE_DELAY_MS
#define TOOGLE_DELAY_MS            1000000
#endif
/***************************************************************************//**
 * Initialize application.
 ******************************************************************************/
void app_init(void)
{
  // Initialization of USTIMER driver
      USTIMER_Init();

      sl_led_init(&sl_led_led0);

      initUSART1();
      spidrv_app_zoe_init();

}

/***************************************************************************//**
 * App ticking function.
 ******************************************************************************/
void app_process_action(void)
{
  // Wait for 1000 microseconds
      USTIMER_Delay(TOOGLE_DELAY_MS);
      // toggle LED
      sl_led_toggle(&LED_INSTANCE);

      spidrv_app_zoe_action();
}
